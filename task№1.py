"""
Угадать слово
"""

secret_word = 'машина'
attempt = 5
user_chars = []

for i in range(len(secret_word)):
    print('*', end=' ')

while 1:
    print('')
    is_win = True

    user_char = input('Введите букву: ').lower()
    if user_char not in user_chars:
        user_chars.append(user_char)
    for char in secret_word:
        if char in user_chars:
            print(char, end=' ')
        else:
            print('*', end='')
            is_win = False

    if user_char not in secret_word:
        attempt = -1
        print(f'Потрачена {attempt} попытка')

    if attempt == 0:
        print('Поражение')
        break
    elif is_win is True :
        print('Победа')
        break
